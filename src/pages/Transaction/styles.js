import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
    fabFixed: {
        margin: 0,
        top: 'auto',
        right: 20,
        bottom: 30,
        left: 'auto',
        position: 'fixed'
    }
}));

export default useStyles;
