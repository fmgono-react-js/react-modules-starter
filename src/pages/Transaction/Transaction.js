import React, { useEffect } from 'react';
import { NavLink } from 'react-router-dom';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import AddIcon from '@material-ui/icons/Add';
import Fab from '@material-ui/core/Fab';

import styles from './styles';

const Transaction = props => {
    const classes = styles();
    const { setTitle } = props;
    useEffect(() => setTitle('Transaction'));


    const createData = (no, transactionNumber, transactionRemark,transactionAmount) => {
        return {no, transactionNumber, transactionRemark, transactionAmount};
    };

    const data = [
        createData(1, 'KK-JKT20190001', 'KETERANGAN 1', '1.000.000'),
        createData(2, 'KK-JKT20190002', 'KETERANGAN 2', '2.000.000'),
        createData(3, 'KK-JKT20190003', 'KETERANGAN 3', '3.000.000'),
        createData(4, 'KK-JKT20190004', 'KETERANGAN 4', '4.000.000')
    ]

    return (
        <>
            <Paper>
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell>No</TableCell>
                            <TableCell>Nomor Transaksi</TableCell>
                            <TableCell>Keterangan Transaksi</TableCell>
                            <TableCell>Nilai Transaksi</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {data.map( data => (
                            <TableRow key={data.no}>
                                <TableCell>{data.no}</TableCell>
                                <TableCell>{data.transactionNumber}</TableCell>
                                <TableCell>{data.transactionRemark}</TableCell>
                                <TableCell>{data.transactionAmount}</TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </Paper>
            <Fab
                color="primary"
                aria-label="add"
                className={classes.fabFixed}>
                <NavLink to="/transaction/add">
                    <AddIcon />
                </NavLink>
            </Fab>
        </>
    )
};

export default Transaction;
