import React, { useEffect } from 'react';
import Typography from '@material-ui/core/Typography';

const Dashboard = props => {
    const { setTitle } = props;
    useEffect(() => setTitle('Dashboard'));
    return (
        <Typography variant="h5">
            Dashboard Page
        </Typography>
    )
};

export default Dashboard;
