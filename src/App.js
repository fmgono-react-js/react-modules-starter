import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';

import Layouts from './layouts/Layouts';

const App = () => {
  return (
    <Router>
        <Layouts/>
    </Router>
  );
}

export default App;
