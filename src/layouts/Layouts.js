import React, { useState } from 'react';
import { NavLink } from 'react-router-dom';
import CssBaseline from '@material-ui/core/CssBaseline';
import SideDrawer from './SideDrawer/SideDrawer';
import Content from './Content/Content';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import InboxIcon from '@material-ui/icons/MoveToInbox';

import styles from './styles';
import TopBar from './TopBar/TopBar';

const menusTree = [
    {
        title: 'Dashboard',
        link: '/'
    },
    {
        title: 'Transaction',
        link: '/transaction'
    }
];

const Layouts = props => {
    const classes = styles();
    const [isOpen, setOpen] = useState(false);
    const [titlePage, setTitlePage] = useState('');
    const toggleDrawer = () => setOpen(!isOpen);
    const setTitle = (title) => setTitlePage(title);
    return (
        <div className={classes.root}>
            <CssBaseline />
            <TopBar
                titlePage={titlePage}
                isOpen={isOpen}
                toggleDrawer={toggleDrawer} />
            <SideDrawer isOpen={isOpen}>
                {menusTree.map((menu, index) => (
                <NavLink
                    exact={menu.link === '/' ? true : false}
                    activeStyle={{color: '#3f51b5'}}
                    className={classes.link}
                    to={menu.link}
                    key={index}>
                    <ListItem button>
                        <ListItemIcon>
                            <InboxIcon />
                        </ListItemIcon>
                    <ListItemText primary={menu.title} />
                    </ListItem>
                </NavLink>
                ))}
            </SideDrawer>

            <Content
                isOpen={isOpen}
                setTitle={setTitle}
                titlePage={titlePage}
                toggleDrawer={toggleDrawer} />

        </div>
    )
};

export default Layouts;
