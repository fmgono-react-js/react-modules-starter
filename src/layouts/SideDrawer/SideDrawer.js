import React from 'react';
import Drawer from '@material-ui/core/Drawer';
import IconButton from '@material-ui/core/IconButton';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';

import styles from './styles';

const SideDrawer = props => {
    const classes = styles();
    const {isOpen} = props;
    return (
        <Drawer
           className={classes.drawer}
           variant="persistent"
           anchor="left"
           classes={{ paper: classes.drawerPaper }}
           open={isOpen}>
            <div className={classes.drawerHeader}>
                <IconButton>
                </IconButton>
            </div>
            <Divider />
            <List>
                {props.children}
            </List>
        </Drawer>
    )
};

export default SideDrawer;
