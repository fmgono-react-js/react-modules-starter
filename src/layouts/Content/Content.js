import React from 'react';
import clsx from 'clsx';
import { Route, Switch } from 'react-router-dom';

import styles from './styles';
import Dashboard from '../../pages/Dashboard/Dashboard';
import Transaction from '../../pages/Transaction/Transaction';
import TransactionAdd from '../../pages/Transaction/TransactionAdd';



const Content = props => {
    const {isOpen, setTitle, match} = props;
    console.log(match);
    const classes = styles();
    return (
        <main className={clsx(classes.content, {
            [classes.contentShift]: isOpen
        })}>
            <div className={classes.titleBar}></div>
            <Switch>
                <Route exact path="/" render={() => <Dashboard setTitle={setTitle} />} />
                <Route path="/transaction" render={() => <Transaction setTitle={setTitle} />} />
                <Route path="/transaction/add" render={() => <TransactionAdd />} />
            </Switch>
        </main>
    )
};

export default Content;
