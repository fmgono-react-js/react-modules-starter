import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
        flexDirection: 'row'
    },
    link: {
        textDecoration: 'none',
        color: 'rgba(0, 0, 0, 0.87)'
    }
}));

export default useStyles;
